const users = require('./1-users.cjs');

// Q5 Group users based on their Programming language mentioned in their designation.

let languages = ['Python', 'Golang', 'JavaScript'];
let loweredLang = languages.map(element => element.toLowerCase());

let userArray = Object.entries(users);
let groupData = {};

userArray.forEach(([key, data]) => {
  let loweredDesignation = data.desgination.toLowerCase();
  loweredLang.forEach(lang => {
    if (loweredDesignation.includes(lang)) {
      if (!groupData[lang]) {
        groupData[lang] = [];
      }
      groupData[lang].push(key);
    }
  });
});

console.log(groupData);