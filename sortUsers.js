const users = require("./1-users.cjs");

// Q3 Sort users based on their seniority level
// for Designation - Senior Developer > Developer > Intern
// for Age - 20 > 10

const userArray = Object.entries(users);
//I will add one more value to the data which is position which will be according to their designation

let newObj = userArray.map(([key, data]) => {
  data.desgination = data.desgination.toLowerCase();
  if (data.desgination.includes("senior")) {
    data.position = 1;
  } else if (data.desgination.includes("developer")) {
    data.position = 2;
  } else {
    data.position = 3;
  }
  return data;
});

// console.log(newObj);

newObj.sort((user1, user2) => {
  if (user1.position > user2.position) {
    return 1;
  } else if (user1.position < user2.position) {
    return -1;
  } else {
    if (user1.age > user2.age) {
      return -1;
    } else if (user1.age < user2.age) {
      return 1;
    }
  }
});
console.log(newObj);

